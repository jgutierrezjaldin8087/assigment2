import java.util.Scanner;

public class Assigment2
{
	public static void main(String[] args)
	{
		System.out.println("Enter five whole numbers please:");
		int n1, n2, n3, n4, n5;
		
		Scanner keyboard = new Scanner(System.in);
		n1 = keyboard.nextInt();
		n2 = keyboard.nextInt();
		n3 = keyboard.nextInt();
		n4 = keyboard.nextInt();
		n5 = keyboard.nextInt();

		double average;
		average = (n1 + n2 + n3 + n4 + n5) / 5;
		
	

		//Calculates the Sum of those numbers
		System.out.println("The sum of those whole five numbers are ");
		System.out.println( n1 + n2 + n3 + n4 + n5);
		
		//Calculates the average of those numbers
		System.out.println("You entered "  + n1 + ", " + n2+ ", " + n3+ ", " + n4+ ", " + " and " + n5);
		System.out.println("The average of those whole five numbers are ");
		System.out.println( average );

		//Calculates the max
		int max = Math.max(Math.max(Math.max(n1, n2), Math.max(n3,n4)), n5);
		System.out.println("The max number is " + max);
		
		//Calculates the Min
		int min = Math.min(Math.min(Math.min(n1, n2), Math.min(n3,n4)), n5);
		System.out.println("The min number is " + min);
		
	}
}